buttplug.io haptic support for Guilty Gear XX Accent Core Plus R. Recovered from an archived version of https://github.com/super-continent/acpr-buttplug

Created by super-continent.

Probably doesn't work with the latest version of the game because it uses fixed addresses. You're free to try and fix it, though.

## Building

Install Rust, install the nightly toolchain using `rustup toolchain install nightly` and then use `rustup target add --toolchain nightly i686-pc-windows-msvc` to make sure you have the `i686-pc-windows-msvc` target installed for the nightly toolchain.

Run `cargo +nightly build` to build in debug config or `cargo +nightly build --release` to build in release config.